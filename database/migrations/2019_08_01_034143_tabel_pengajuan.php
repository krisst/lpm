<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelPengajuan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan', function (Blueprint $table) {
            $table->increments('id_pengajuan')->index();
            $table->integer('user_id')->unsigned()->index();
            $table->date('tgl_pengajuan')->nullable()->index();
            $table->string('jenis_pengajuan', 20)->nullable();
            $table->smallInteger('status')->unsigned()->default(0);
            $table->string('foto', 200)->nullable();
            $table->string('ktp', 200)->nullable();
            $table->string('ijazah', 200)->nullable();
            $table->string('spkel', 200)->nullable();
            $table->string('spku', 200)->nullable();
            $table->string('sketdl', 200)->nullable();
            $table->string('ssempadan', 200)->nullable();
            $table->string('kk', 200)->nullable();
            $table->string('spr', 200)->nullable();
            $table->string('spaw', 200)->nullable();
            $table->string('spsak', 200)->nullable();
            $table->string('alawybcu', 200)->nullable();
            $table->string('skkdk', 200)->nullable();
            $table->string('sn', 200)->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan');
    }
}
