<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nik', 16)->unique()->index();
            $table->string('no_kk', 20)->nullable()->index();
            $table->string('name', 100)->nullable();
            $table->string('address', 200)->nullable();
            $table->string('rt', 5)->nullable();
            $table->string('rw', 5)->nullable();
            $table->string('kelurahan', 100)->nullable();
            $table->string('kecamatan', 100)->nullable();
            $table->string('phone', 15)->nullable();
            $table->string('gender', 1)->nullable();
            $table->string('religion', 15)->nullable();
            $table->string('ttl', 100)->nullable();
            $table->date('birth_date')->nullable();
            $table->string('status_perkawinan', 100)->nullable();
            $table->string('pekerjaan', 100)->nullable();
            $table->string('kewarganegaraan', 100)->nullable();
            $table->string('email')->unique();
            $table->string('password', 150);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
