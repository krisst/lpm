<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInTablePengajuan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pengajuan', function (Blueprint $table) {
            $table->string('nama_alm', 200)->nullable();
            $table->string('alamat_alm', 200)->nullable();
            $table->date('tgl_meninggal')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengajuan', function (Blueprint $table) {
            $table->dropColumn('nama_alm');
            $table->dropColumn('alamat_alm');
            $table->dropColumn('tgl_meninggal');
        });
    }
}
