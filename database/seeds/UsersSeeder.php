<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // Membuat role admin
		$adminRole = new Role();
		$adminRole->name = "admin";
		$adminRole->display_name = "Admin";
		$adminRole->save();
		// Membuat role warga
		$wargaRole = new Role();
        $wargaRole->name = "warga";
        $wargaRole->display_name = "Warga";
        $wargaRole->save();
        
        $admin = new User();
        $admin->nik = "1233545423";
        $admin->name = 'Admin';
        $admin->email = 'admin@gmail.com';
        $admin->password = bcrypt('rahasia');
        $admin->is_verified = 1;
        $admin->save();
        $admin->attachRole($adminRole);

        // Membuat sample warga
        $warga = new User();
		$warga->nik = "1234545458";
        $warga->name = "Tatang Sudrajat";
        $warga->email = 'tatangs@gmail.com';
        $warga->password = bcrypt('rahasia');
        $warga->is_verified = 1;
        $warga->save();
        $warga->attachRole($wargaRole);
		
    }
}

