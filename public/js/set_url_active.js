var url = window.location.href;
// for sidebar menu but not for treeview submenu
$('ul.nav a').filter(function() {
	var url_a = String(this.href);
	if(String(url).indexOf(url_a) != -1){
    	return this.href == url_a;
	}
}).parent().siblings().removeClass('active').end().addClass('active');
// for treeview which is like a submenu
$('ul.navbar-nav a').filter(function() {
    var url_a = String(this.href);
	if(String(url).indexOf(url_a) != -1){
    	return this.href == url_a;
	}
}).parentsUntil(".nav > .navbar-nav").siblings().removeClass('active menu-open').end().addClass('active menu-open');
