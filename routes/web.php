<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

// Route::get('/login', function () {
//     return view('welcome');
// })->name('login');


// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/load_countries', 'CountriesController@loadCountries')->name('load_countries');

Route::group(['middleware'=> 'auth'], function(){
	Route::resource('pengajuan', 'PengajuanController');
	Route::get('/pengajuan/print/{id}', 'PengajuanController@printSurat')->name('pengajuan.print');
	Route::post('/pengajuan/skm', 'DaftarPengajuanController@skm')->name('pengajuan_skm');
	Route::post('/pengajuan/skdtt', 'DaftarPengajuanController@skdtt')->name('pengajuan_skdtt');
	Route::post('/pengajuan/sku', 'DaftarPengajuanController@sku')->name('pengajuan_sku');
});

Route::group(['prefix'=>'admin', 'middleware'=>['auth','role:admin']], function () {
	Route::get('/master/warga', 'WargaController@index')->name('warga.index');
	Route::get('/master/warga/{id_pengajuan}/detail', 'WargaController@show')->name('warga.show');
	Route::post('/pengajuan/proses', 'PengajuanController@proses')->name('pengajuan.proses');
	Route::get('/pengajuan/tolak/{id_pengajuan}', 'PengajuanController@tolak')->name('pengajuan.tolak');
	//Pengajuan
});
