<div class="modal fade" id="modal-skdu">
  	<div class="modal-dialog">
        <div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		      		<span aria-hidden="true">&times;</span>
		  		</button>
		    	<h4 class="modal-title">Persyaratan Surat Keterangan Domisili Usaha</h4>
		  	</div>
		  	<div class="modal-body">
		        <div class="row">
		        	<div class="col-md-12 table-responsive">
		        		<p>
		        			Surat yang menyatakan domisili seseorang atau suatu badan usaha/perusahaan.
		        		</p><br>
		        		<table class="table table-condensed table-hover table-striped table-bordered">
		        			<thead>
		        				<tr>
		        					<th width="5%" class="text-right">No.</th>
		        					<th width="15%" class="text-center">Ketentuan</th>
		        					<th>Syarat</th>
		        				</tr>
		        			</thead>
		        			<tbody>
		        				<tr>
		        					<td class="text-right">1.</td>
		        					<td class="text-center">Wajib</td>
		        					<td>Pas Foto 2x3 cm 2 lembar</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">2.</td>
		        					<td class="text-center">Wajib</td>
		        					<td>Scan KTP</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">3.</td>
		        					<td class="text-center">Wajib</td>
		        					<td>Surat Pengantar Kelurahan</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">4.</td>
		        					<td class="text-center">Wajib</td>
		        					<td>Surat Pernyataan Kepemilikan Usaha (Materai Rp. 6.000,-)</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">5.</td>
		        					<td class="text-center">Wajib</td>
		        					<td>Sketsa Denah Lokasi</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">6.</td>
		        					<td class="text-center">Wajib</td>
		        					<td>Surat Sempadan Diketahui Oleh Lurah Setempat (Toko Bangunan, Salon, Rumah makan, Bilyard kecuali permainan elektronik tidak boleh dilokasi perumahan)</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">7.</td>
		        					<td class="text-center">Tidak Wajib</td>
		        					<td>Surat Kepemilikan Tempat/Perjanjian Sewa Menyewa Usaha</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">8.</td>
		        					<td class="text-center">Tidak Wajib</td>
		        					<td>Akte Pendirian Usaha bagi Pemohon Berbadan Hukum</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">9.</td>
		        					<td class="text-center">Tidak Wajib</td>
		        					<td>Paspor/KITAS bagi Pemohon Asing (WNA)</td>
		        				</tr>
		        			</tbody>
		        		</table>
		  			</div>
		        </div>
		  	</div>
		    <div class="modal-footer">
		        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
		    </div>
    
    	</div>
  	</div>
</div>