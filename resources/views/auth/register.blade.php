@extends('templates.base')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-2">

            </div>
            <div class="col-xs-8">
                <div class="box box-primary">
                    <div class="box-header text-center">
                        <h3 class="box-title">FORM REGISTRASI WARGA</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <div class="form-group col-md-12 form-group-md">
                                <label class="form-control-static col-md-3">NIK *</label>
                                <div class="form-group col-md-9">
                                    <input type="text" class="form-control number" name="nik" required>
                                    @if ($errors->has('nik'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('nik') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group col-md-12 form-group-md">
                                <label class="form-control-static col-md-3">Nama Lengkap *</label>
                                <div class="form-group col-md-9">
                                    <input type="text" class="form-control" name="name" required>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group col-md-12 form-group-md">
                                <label class="form-control-static col-md-3">Jenis Kelamin *</label>
                                <div class="form-group col-md-9">
                                    <label class="form-control-static col-md-2">
                                        <input type="radio" name="gender" value="L" class="minimal" checked> L
                                    </label>
                                    <label class="form-control-static col-md-2">
                                        <input type="radio" name="gender" value="P" class="minimal"> P
                                    </label>
                                    @if ($errors->has('gender'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('gender') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group col-md-12 form-group-md">
                                <label class="form-control-static col-md-3">Tempat Lahir *</label>
                                <div class="form-group col-md-3">
                                    <input type="text" class="form-control" name="ttl" required>
                                    @if ($errors->has('ttl'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('ttl') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <label class="form-control-static col-md-3">Tanggal Lahir *</label>
                                <div class="form-group col-md-3">
                                    <input type="text" class="form-control datepicker" name="birth_date" required>
                                    @if ($errors->has('birth_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('birth_date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group col-md-12 form-group-md">
                                <label class="form-control-static col-md-3">Kewarganegaraan *</label>
                                <div class="form-group col-md-5">
                                    <select name="country_id" id="" class="loadCountries form-control" style="width: 100%;" required></select>
                                </div>
                            </div>
                            <div class="form-group col-md-12 form-group-md">
                                <label class="form-control-static col-md-3">Agama *</label>
                                <div class="form-group col-md-5">
                                    <select class="form-control select-option" name="religion">
                                        <option></option>
                                        <option value="Islam">Islam</option>
                                        <option value="Kristen Protestan">Kristen Protestan</option>
                                        <option value="Kristen Katholik">Kristen Katholik</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Budha">Budha</option>
                                        <option value="Komuchu">Komuchu</option>
                                    </select>
                                    @if ($errors->has('religion'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('religion') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group col-md-12 form-group-md">
                                <label class="form-control-static col-md-3">Status Perkawinan *</label>
                                <div class="form-group col-md-5">
                                    <select class="form-control select-option" name="status_perkawinan">
                                        <option></option>
                                        <option value="Kawin">Kawin</option>
                                        <option value="Lajang">Lajang</option>
                                        <option value="Duda">Duda</option>
                                        <option value="Janda">Janda</option>
                                    </select>
                                    @if ($errors->has('religion'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('religion') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group col-md-12 form-group-md">
                                <label class="form-control-static col-md-3">Pekerjaan *</label>
                                <div class="form-group col-md-5">
                                    <input type="text" class="form-control" name="pekerjaan" required>
                                    @if ($errors->has('pekerjaan'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('pekerjaan') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group col-md-12 form-group-md">
                                <label class="form-control-static col-md-3">Alamat *</label>
                                <div class="form-group col-md-5">
                                    <textarea class="form-control" rows="2" name="address" required></textarea>
                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group col-md-12 form-group-md">
                                <label class="form-control-static col-md-3"> RT / RW * </label>
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control number" name="rt" required>
                                </div>
                                <label class="form-control-static col-md-1 text-center">/</label>
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control number" name="rw" required>
                                </div>
                            </div>

                            <div class="form-group col-md-12 form-group-md">
                                <label class="form-control-static col-md-3">Kelurahan * </label>
                                <div class="form-group col-md-3">
                                    <input type="text" class="form-control" name="kelurahan" required>
                                </div>
                                <label class="form-control-static col-md-3">Kecamtan *</label>
                                <div class="form-group col-md-3">
                                    <input type="text" class="form-control" name="kecamatan" required>
                                </div>
                            </div>

                            <div class="form-group col-md-12 form-group-md">
                                <label class="form-control-static col-md-3">No. Telp / HP *</label>
                                <div class="form-group col-md-5">
                                    <input type="text" class="form-control number" name="phone" required>
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group col-md-12 form-group-md">
                                <label class="form-control-static col-md-3">Email *</label>
                                <div class="form-group col-md-5">
                                    <input type="email" class="form-control" name="email" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group col-md-12 form-group-md">
                                <label class="form-control-static col-md-3">Kata Sandi *</label>
                                <div class="form-group col-md-3">
                                    <input type="password" class="form-control" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <label class="form-control-static col-md-3">Ulang Kata Sandi *</label>
                                <div class="form-group col-md-3">
                                    <input type="password" class="form-control" name="password_confirmation" required>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group col-md-12 form-group-md">
                                <label class="form-control-static col-md-3"></label>
                                <div class="form-group col-md-9">
                                    <br>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> Daftar</button>
                                    <a class="btn btn-danger" href=""><i class="fa fa-close"></i> Cancel</a>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                  <!-- /.box -->
            </div>
            <div class="col-xs-2">

            </div>
        </div>
    </section>
@endsection
