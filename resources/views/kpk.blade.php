<div class="modal fade" id="modal-kpk">
  	<div class="modal-dialog">
        <div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		      		<span aria-hidden="true">&times;</span>
		  		</button>
		    	<h4 class="modal-title">Persyaratan Kartu Pencari Kerja</h4>
		  	</div>
		  	<div class="modal-body">
		        <div class="row">
		        	<div class="col-md-12 table-responsive">
		        		<p>
		        			Kartu yang diisyaratkan untuk mendapatkan pekerjaan serta mendata tingkat pengangguran. Kartu ini hanya berlaku 6 bulan saja.
		        		</p><br>
		        		<table class="table table-condensed table-hover table-striped table-bordered">
		        			<thead>
		        				<tr>
		        					<th width="5%" class="text-right">No.</th>
		        					<th width="15%" class="text-center">Ketentuan</th>
		        					<th>Syarat</th>
		        				</tr>
		        			</thead>
		        			<tbody>
		        				<tr>
		        					<td class="text-right">1.</td>
		        					<td class="text-center">Wajib</td>
		        					<td>Pas Foto 2x3 cm 2 lembar</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">2.</td>
		        					<td class="text-center">Wajib</td>
		        					<td>Scan KTP</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">3.</td>
		        					<td class="text-center">Wajib</td>
		        					<td>Scan Ijazah</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">4.</td>
		        					<td class="text-center">Tidak Wajib</td>
		        					<td>Scan Kartu Keluarga (KK)</td>
		        				</tr>
		        			</tbody>
		        		</table>
		  			</div>
		        </div>
		  	</div>
		    <div class="modal-footer">
		        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
		    </div>
    
    	</div>
  	</div>
</div>