<div class="modal fade" id="modal-daftar-kpk">
  	<div class="modal-dialog">
        <div class="modal-content">
		  	<div class="modal-header bg-orange">
		    	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		      		<span aria-hidden="true">&times;</span>
		  		</button>
		    	<h4 class="modal-title">Daftar Kartu Pencari Kerja</h4>
		  	</div>
		  	<div class="modal-body">
		        <form class="form-horizontal" action="{{ route('pengajuan.store') }}" autocomplete="off" method="post" enctype="multipart/form-data">
		        	<div class="row">
		        		<div class="col-md-12">
		        			{{ csrf_field() }}
                            <div class="form-group col-md-12 form-group-md">

                            	<div class="col-md-3">
						            <!-- Profile Image -->
						            <div class="box box-danger">
						                <div class="box-body box-profile">
						                  	<center><label class="form-control-static">Foto 2 x 3</label></center>
		                                	<img src="{{ asset('img/bg/img.png') }}" class="img-responsive">
		                                    <input type="file" class="form-control" name="foto" required>
		                                    @if ($errors->has('foto'))
		                                        <span class="help-block">
		                                            <strong>{{ $errors->first('foto') }}</strong>
		                                        </span>
		                                    @endif
						                </div>
						                <!-- /.box-body -->
						            </div>
						        </div>
						        <div class="col-md-3">
						            <!-- Profile Image -->
						            <div class="box box-danger">
						                <div class="box-body box-profile">
						                  	<center><label class="form-control-static">Scan KTP</label></center>
		                                	<img src="{{ asset('img/bg/img.png') }}" class="img-responsive">
		                                    <input type="file" class="form-control" name="ktp" required>
		                                    @if ($errors->has('ktp'))
		                                        <span class="help-block">
		                                            <strong>{{ $errors->first('ktp') }}</strong>
		                                        </span>
		                                    @endif
						                </div>
						                <!-- /.box-body -->
						            </div>
						        </div>
						        <div class="col-md-3">
						            <!-- Profile Image -->
						            <div class="box box-danger">
						                <div class="box-body box-profile">
						                  	<center><label class="form-control-static">Scan Ijazah</label></center>
		                                	<img src="{{ asset('img/bg/img.png') }}" class="img-responsive">
		                                    <input type="file" class="form-control" name="ijazah" required>
		                                    @if ($errors->has('ijazah'))
		                                        <span class="help-block">
		                                            <strong>{{ $errors->first('ijazah') }}</strong>
		                                        </span>
		                                    @endif
						                </div>
						                <!-- /.box-body -->
						            </div>
						        </div>
						        <div class="col-md-3">
						            <!-- Profile Image -->
						            <div class="box box-danger">
						                <div class="box-body box-profile">
						                  	<center><label class="form-control-static">Scan Kartu Keluarga</label></center>
		                                	<img src="{{ asset('img/bg/img.png') }}" class="img-responsive">
		                                    <input type="file" class="form-control" name="kk" required>
		                                    @if ($errors->has('kk'))
		                                        <span class="help-block">
		                                            <strong>{{ $errors->first('kk') }}</strong>
		                                        </span>
		                                    @endif
		                                    <input type="hidden" name="jenis_pengajuan" value="kpk">
						                </div>
						                <!-- /.box-body -->
						            </div>
						        </div>
                            </div>
                            <div class="col-md-12">
                            	<br>
                            	<br>
                            	<br>
                            	<center>
                            		<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                            		<button class="btn btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i> Batalkan</button>
                            	</center>
                            </div>
		        		</div>
		        	</div>
		        </form>
		  	</div>
    	</div>
  	</div>
</div>