@extends('templates.print_slip')

@section('content')
<table  width="100%">
	<tr align="center">
		<td align="" colspan="3">
			<p style="">
				<img src="{{ asset('img/logo/bb.png') }}" style="max-height: 65px;float:left; ">PEMERINTAH KABUPATEN BANDUNG BARAT KECAMATAN LEMBANG<br>Jalan Kayu Ambon No. 65 Lembang Kabupaten Bandung Barat 40391 <br>No. Telp : (022) 2786020
			</p>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr align="center">
		<td style="border: 1px solid black;" colspan="3">SURAT KETERANGAN USAHA</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{ $pengajuan->id_pengajuan }}/SKU/{{ date('dmY') }}</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">
			<p class="text-justify">
				Yang bertanda tangan di bawah ini selaku Camat, Kecamatan Lembang, Kabupaten Bandung Barat dengan ini menerangkan bahwa :
			</p>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td width="25%">Nama Lengkap</td>
		<td>: {{ $pengajuan->user->name }}</td>
	</tr>
	<tr>
		<td width="25%">Tempat / Tgl Lahir</td>
		<td>: {{ $pengajuan->user->ttl }}, {{ date('d-m-Y' ,strtotime($pengajuan->user->birth_date)) }}</td>
	</tr>
	<tr>
		<td width="25%">Jenis Kelamin</td>
		<td>: {{ $pengajuan->user->gender }}</td>
	</tr>
	<tr>
		<td width="25%">Agama</td>
		<td>: {{ $pengajuan->user->religion }}</td>
	</tr>
	<tr>
		<td width="25%">Pekerjaan</td>
		<td>: {{ $pengajuan->user->pekerjaan }}</td>
	</tr>
	<tr>
		<td width="25%">Alamat</td>
		<td>: {{ $pengajuan->user->address }}</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">
			<p class="text-justify">
				Menyatakan bahwa nama di atas benar keberadaannya disini sebagai warga kami yang baik di wilayah kami yang pada saat ini berdomisili di Kabupaten Bandung Barat.
			</p>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">
			<p class="text-justify">
				Berdasarkan pengamatan kami bahwa nama tersebut di atas adalah benar memiliki usaha {{ $pengajuan->nama_alm }} yang beralamatkan di {{ $pengajuan->alamat_alm }}.
			</p>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">
			<p class="text-justify">
				Demikianlah surat keterangan ini dibuat dengan sebenarnya dan penuh kesadaran untuk bisa digunakan sebagaimana mestinya digunakan.
			</p>
		</td>
	</tr>
</table>
<table width="100%" style="margin-bottom:0px;">
	<tbody>
		<tr align="right">
			<td class="" colspan="3">
				<p class="">
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					Lembang, {{date('d M Y')}}
					<br>
					Kepala Kelurahan Lembang
					<br>
					<br>
					<br>
					<br>
					<br>
					<u>Drs. Slamet Nugraha, SIP</u><br>
					NIP: 18729192020
				</p>
			</td>
		</tr>
	</tbody>
</table>

@endsection