@extends('templates.base')

@section('content')
    <section class="content-header">
      <h1>
            SURAT PENGAJUAN
            <!-- <small>Control panel</small> -->
        </h1>
      <ol class="breadcrumb">
          <li><a href="{{ route('home') }}"><i class="fa fa-home"></i> Beranda</a></li>
          <li> {{ $title }}</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
          <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">&nbsp;</h3>

                  <div class="box-tools">
                    <!-- <form method="get">
                      <div class="input-group input-group-sm" style="width: 300px;">
                          <input type="text" name="keyword" class="form-control pull-right" value="@if(isset($keyword)){{$keyword}}@endif" placeholder="Search">

                          <div class="input-group-btn">
                              <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                    </form> -->
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                  <!-- <a class="btn btn-sm btn-success" href=""><i class="fa fa-plus"></i> New User</a><br> -->
                  <table id="example1" class="table table-hover table-condensed table-striped">
                      <thead>
                        <tr>
                          <th class="text-right">No</th>
                          <th class="">NIK</th>
                          <th class="">No KK</th>
                          <th>Nama</th>
                          <th>Telp/HP</th>
                          <th>Email</th>
                          <th>Jenis Pengajuan</th>
                          <th>Status</th>
                          <th class="text-center">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?PHP $no = 1; ?>
                      @foreach($pengajuan as $st)
                        <tr>
                          <td class="text-right">{{ $no++ }}</td>
                          <td>{{ $st->user->nik }}</td>
                          <td>{{ $st->user->no_kk }}</td>
                          <td>{{ $st->user->name }}</td>
                          <td>{{ $st->user->phone }}</td>
                          <td>{{ $st->user->email }}</td>
                          <td>{{ $st->jenisPengajuan($st->jenis_pengajuan) }}</td>
                          <td>{{ $st->statusPengajuan($st->status) }}</td>
                          <td class="text-center"><a href="{{ route('pengajuan.show', ['id_pengajuan' => $st->id_pengajuan]) }}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a></td>
                        </tr>
                      @endforeach
                      </tbody>
                  </table>

                </div>
                <!-- /.box-body -->
            </div>
              <!-- /.box -->
          </div>
      </div>
    </section>
@endsection

