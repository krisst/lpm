@extends('templates.base')

@section('content')
    <section class="content-header">
      <h1>
            {{$title}}
            <!-- <small>Control panel</small> -->
        </h1>
      <ol class="breadcrumb">
          <li><a href="{{ route('home') }}"><i class="fa fa-home"></i> Beranda</a></li>
          <li> {{ $title }}</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
          <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header text-center">
                  <h3 class="box-title">
                    @if($pengajuan->status == 1)
                      <button class="btn btn-success"><i class="fa fa-check"></i> PENGAJUAN DISETUJUI</button></center>
                    @elseif(($pengajuan->status == 2))
                      <button class="btn btn-danger"><i class="fa fa-close"></i> PENGAJUAN DITOLAK</button></center>
                    @else
                      &nbsp;
                    @endif
                  </h3>

                  <div class="box-tools">
                    <!-- <form method="get">
                      <div class="input-group input-group-sm" style="width: 300px;">
                          <input type="text" name="keyword" class="form-control pull-right" value="@if(isset($keyword)){{$keyword}}@endif" placeholder="Search">

                          <div class="input-group-btn">
                              <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                    </form> -->
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                  <!-- <a class="btn btn-sm btn-success" href=""><i class="fa fa-plus"></i> New User</a><br> -->
                  <div class="col-md-6">
                    Tanggal &nbsp;&nbsp;&nbsp;&nbsp;: {{ date('d-m-Y', strtotime($pengajuan->tgl_pengajuan)) }}
                    <br><br><br>
                    Diajukan oleh :<br><br>
                    <table id="" class="table table-hover table-condensed">
                        <tbody>
                        <tr>
                          <td width="33%">No. KK</td>
                          <td width="">: {{ $pengajuan->user->no_kk }}</td>
                        </tr>
                        <tr>
                          <td width="33%">NIK</td>
                          <td width="">: {{ $pengajuan->user->nik }}</td>
                        </tr>
                        <tr>
                          <td width="33%">Nama Lengkap</td>
                          <td width="">: {{ $pengajuan->user->name }}</td>
                        </tr>
                        <tr>
                          <td width="33%">Tempat/Tanggal Lahir</td>
                          <td width="">: {{ $pengajuan->user->ttl }}, {{ date('d M Y', strtotime($pengajuan->user->birth_date)) }}</td>
                        </tr>
                        <tr>
                          <td width="33%">Jenis Kelamin</td>
                          <td width="">: {{ $pengajuan->user->gender }}</td>
                        </tr>
                        <tr>
                          <td width="33%">Agama</td>
                          <td width="">: {{ $pengajuan->user->religion }}</td>
                        </tr>
                        <tr>
                          <td width="33%">Address</td>
                          <td width="">: {{ $pengajuan->user->address }}</td>
                        </tr>
                        <tr>
                          <td width="33%">No. Telp / HP</td>
                          <td width="">: {{ $pengajuan->user->phone }}</td>
                        </tr>
                        <tr>
                          <td width="33%">Email</td>
                          <td width="">: {{ $pengajuan->user->email }}</td>
                        </tr>
                        </tbody>
                    </table>
                    
                    @if($pengajuan->jenis_pengajuan == 'skm')
                    Yang telah meninggal dunia :<br><br>
                    <table class="table table-hover table-condensed">
                      <tbody>
                        <tr>
                          <td width="33%">Nama Lengkap Alm</td>
                          <td width="">: {{ $pengajuan->nama_alm }}</td>
                        </tr>
                        <tr>
                          <td width="33%">Alamat Alm</td>
                          <td width="">: {{ $pengajuan->alamat_alm }}</td>
                        </tr>
                        <tr>
                          <td width="33%">Tanggal Meninggal</td>
                          <td width="">: {{ $pengajuan->tgl_meninggal }}</td>
                        </tr>
                      </tbody>
                    </table>
                    @endif

                    @role('admin')
                    @if($pengajuan->status == 0)
                    <form class="form-horizontal" method="post" action="{{ route('pengajuan.proses') }}" onsubmit="return confirm('Anda yakin untuk memproses pengajuan ini?')">
                      {{ csrf_field() }}
                      <input type="hidden" name="id_pengajuan" value="{{ $pengajuan->id_pengajuan }}">
                      <div class="col-md-12">
                        <div class="form-group">
                          <button class="btn btn-success"><i class="fa fa-check"></i> Proses Pengajuan</button>
                          <a onclick="return confirm('Anda yakin untuk menolak pengajuan ini?')" class="btn btn-danger" href="{{ route('pengajuan.tolak', ['id_pengajuan' => $pengajuan->id_pengajuan]) }}"><i class="fa fa-close"></i> Tolak Pengajuan</a>
                        </div>
                      </div>
                    </form>
                    @elseif($pengajuan->status == 1)
                      <div class="">
                        <a class="btn btn-info" target="_blank" href="{{ route('pengajuan.print', $pengajuan->id_pengajuan) }}"><i class="fa fa-print"></i> Cetak Surat</a>
                      </div>
                    @endif
                    @endrole
                    @role('warga')
                    @if($pengajuan->status == 1)
                      <div class="">
                        <a class="btn btn-info" target="_blank" href="{{ route('pengajuan.print', $pengajuan->id_pengajuan) }}"><i class="fa fa-print"></i> Cetak Surat</a>
                      </div>
                    @endif
                    @endrole
                    
                  </div>
                  <div class="col-md-6">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <br><br><br>
                    Lampiran :<br><br>

                    <ul class="mailbox-attachments clearfix">
                      @if(isset($pengajuan->foto))
                        <li>
                          <a href="{{ asset('gambar/berkas/'.$pengajuan->foto) }}" target="_blank">
                              <span class="mailbox-attachment-icon"><img src="{{ asset('gambar/berkas/'.$pengajuan->foto) }}" style="max-height: 100px;"></span>
                          </a>
                          <div class="mailbox-attachment-info">
                            <span class="mailbox-attachment-size">
                              Foto
                            </span>
                          </div>
                        </li>
                      @endif
                      @if(isset($pengajuan->ktp))
                        <li>
                          <a href="{{ asset('gambar/berkas/'.$pengajuan->ktp) }}" target="_blank">
                              <span class="mailbox-attachment-icon"><img src="{{ asset('gambar/berkas/'.$pengajuan->ktp) }}" style="max-height: 100px;"></span>
                          </a>
                          <div class="mailbox-attachment-info">
                            <span class="mailbox-attachment-size">
                              KTP
                            </span>
                          </div>
                        </li>
                      @endif
                      @if(isset($pengajuan->kk))
                        <li>
                          <a href="{{ asset('gambar/berkas/'.$pengajuan->kk) }}" target="_blank">
                              <span class="mailbox-attachment-icon"><img src="{{ asset('gambar/berkas/'.$pengajuan->kk) }}" style="max-height: 100px;"></span>
                          </a>
                          <div class="mailbox-attachment-info">
                            <span class="mailbox-attachment-size">
                              Kartu Keluarga
                            </span>
                          </div>
                        </li>
                      @endif
                      @if(isset($pengajuan->ijazah))
                        <li>
                          <a href="{{ asset('gambar/berkas/'.$pengajuan->ijazah) }}" target="_blank">
                              <span class="mailbox-attachment-icon"><img src="{{ asset('gambar/berkas/'.$pengajuan->ijazah) }}" style="max-height: 100px;"></span>
                          </a>
                          <div class="mailbox-attachment-info">
                            <span class="mailbox-attachment-size">
                              Ijazah
                            </span>
                          </div>
                        </li>
                      @endif
                      @if(isset($pengajuan->spr))
                        <li>
                          <a href="{{ asset('gambar/berkas/'.$pengajuan->spr) }}" target="_blank">
                              <span class="mailbox-attachment-icon"><img src="{{ asset('gambar/berkas/'.$pengajuan->spr) }}" style="max-height: 100px;"></span>
                          </a>
                          <div class="mailbox-attachment-info">
                            <span class="mailbox-attachment-size">
                              Surat Pengantar Rt/Rw
                            </span>
                          </div>
                        </li>
                      @endif
                    </ul>
                  </div>

                </div>
                <!-- /.box-body -->
            </div>
              <!-- /.box -->
          </div>
      </div>
    </section>
@endsection

