@extends('templates.print_slip')

@section('content')
<table  width="100%">
	<tr align="center">
		<td align="" colspan="2">
			<p style="">
				<img src="{{ asset('img/logo/bb.png') }}" style="max-height: 65px;float:left; ">PEMERINTAH KABUPATEN BANDUNG BARAT KECAMATAN LEMBANG<br>Jalan Kayu Ambon No. 65 Lembang Kabupaten Bandung Barat 40391 <br>No. Telp : (022) 2786020
			</p>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr align="center">
		<td style="border: 1px solid black;" colspan="2">KARTU TANDA BUKTI PENDAFTARAN KERJA</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td width="40%">No. Pendaftaran Pencari Kerja</td>
		<td>: {{ date('Ymd', strtotime($pengajuan->tgl_pengajuan)) }}/SP/KPK/{{ $pengajuan->id_pengajuan }}</td>
	</tr>
	<tr>
		<td width="40%">No. Induk Kependudukan</td>
		<td>: {{ $pengajuan->user->nik }}</td>
	</tr>
	<tr>
		<td width="40%">Nama Lengkap</td>
		<td>: {{ $pengajuan->user->name }}</td>
	</tr>
	<tr>
		<td width="40%">Tempat / Tgl Lahir</td>
		<td>: {{ $pengajuan->user->ttl }}, {{ date('d-m-Y' ,strtotime($pengajuan->user->birth_date)) }}</td>
	</tr>
	<tr>
		<td width="40%">Jenis Kelamin</td>
		<td>: {{ $pengajuan->user->gender }}</td>
	</tr>
	<tr>
		<td width="40%">Agama</td>
		<td>: {{ $pengajuan->user->religion }}</td>
	</tr>
	<tr>
		<td width="40%">Alamat</td>
		<td>: {{ $pengajuan->user->address }}</td>
	</tr>
</table>
<br>
<div style="border: 1px solid black; width: 3cm; height: 4cm; text-align: center; line-height: 4cm">
	3 x 4 cm
</div>
@endsection