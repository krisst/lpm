@extends('templates.print_slip')

@section('content')
<table  width="100%">
	<tr align="center">
		<td align="" colspan="3">
			<p style="">
				<img src="{{ asset('img/logo/bb.png') }}" style="max-height: 65px;float:left; ">PEMERINTAH KABUPATEN BANDUNG BARAT KECAMATAN LEMBANG<br>Jalan Kayu Ambon No. 65 Lembang Kabupaten Bandung Barat 40391 <br>No. Telp : (022) 2786020
			</p>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr align="center">
		<td style="border: 1px solid black;" colspan="3">SURAT KETERANGAN KEMATIAN</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">Yang bertangguang jawab dibawah  ini: </td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td width="15%">Nama</td>
		<td>: Drs. Slamet Nugraha, SIP</td>
		<td></td>
	</tr>
	<tr>
		<td width="15%">NIP</td>
		<td>: 18729192020</td>
		<td></td>
	</tr>
	<tr>
		<td width="15%">Jabatan</td>
		<td>: Kepala Kelurahan Kabupaten Bandung Barat</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">
			<p class="text-justify">
				Menerangkan dengan sebenarnya bahwa nama {{ $pengajuan->nama_alm }} meninggal pada tanggal {{ date('d M Y', strtotime($pengajuan->tgl_meninggal)) }}. Demikian surat keterangan Kematian ini dibuat dengan sebenarnya, kepada yang berwajib dapat maklum.
			</p>
		</td>
	</tr>
</table>
<table width="100%" style="margin-bottom:0px;">
	<tbody>
		<tr align="right">
			<td class="" colspan="3">
				<p class="">
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					<br>
					Lembang, {{date('d M Y')}}
					<br>
					Kepala Kelurahan Lembang
					<br>
					<br>
					<br>
					<br>
					<br>
					<u>Drs. Slamet Nugraha, SIP</u><br>
					NIP: 18729192020
				</p>
			</td>
		</tr>
	</tbody>
</table>

@endsection