@extends('templates.base')

@section('content')

    
    <div class="callout callout-warning">
        @if (Auth::guest())
            <h4>Layanan Masyarakat</h4>
            <p>
                Website ini bertujuan untuk memberikan pelayanan kepada masyarakat setempat untuk kepengurusan surat perizinan dan surat domisili, dengan adanya web ini kami harap bisa meningkatkan pelayan kami dan mempermudah dalam hal kepengurusan perizinan.
            </p>
        @else
            <h4>Selamat Datang, {{ Auth::user()->name }}</h4>
            @role('warga')
              <p>
                Pergunakanlah Layanan kami dengan sebaik baiknya dengan penuh tanggung jawab, SILAHKAN AJUKAN SURAT PADA LAYANAN KAMI SESUAI KEBUTUHAN ANDA.
              </p>
            @endrole
        @endif
    </div>
    <div class="row">
        @role('admin')
          <div class="col-md-4">
              <div class="info-box">
                  <span class="info-box-icon bg-aqua"><i class="fa fa-book"></i></span>

                  <div class="info-box-content">
                      <span class="info-box-text">Jumlah Warga</span>
                      <span class="info-box-number">2</span>
                  </div>
                  <!-- /.info-box-content -->
              </div>
            <!-- /.info-box -->
          </div>
          <div class="col-md-4">
              <div class="info-box">
                  <span class="info-box-icon bg-orange"><i class="fa fa-book"></i></span>

                  <div class="info-box-content">
                      <span class="info-box-text">Jumlah Pengajuan Hari Ini</span>
                      <span class="info-box-number">2</span>
                  </div>
                  <!-- /.info-box-content -->
              </div>
            <!-- /.info-box -->
          </div>
          <div class="col-md-4">
              <div class="info-box">
                  <span class="info-box-icon bg-green"><i class="fa fa-book"></i></span>

                  <div class="info-box-content">
                      <span class="info-box-text">Total Pengajuan</span>
                      <span class="info-box-number">5</span>
                  </div>
                  <!-- /.info-box-content -->
              </div>
            <!-- /.info-box -->
          </div>
          <div class="col-md-4">
              <div class="info-box">
                  <span class="info-box-icon bg-blue"><i class="fa fa-book"></i></span>

                  <div class="info-box-content">
                      <span class="info-box-text">Total Pengajuan yg disetujui</span>
                      <span class="info-box-number">3</span>
                  </div>
                  <!-- /.info-box-content -->
              </div>
            <!-- /.info-box -->
          </div>
          <div class="col-md-4">
              <div class="info-box">
                  <span class="info-box-icon bg-red"><i class="fa fa-book"></i></span>

                  <div class="info-box-content">
                      <span class="info-box-text">Total Pengajuan yg ditolak</span>
                      <span class="info-box-number">2</span>
                  </div>
                  <!-- /.info-box-content -->
              </div>
            <!-- /.info-box -->
          </div>
        @endrole
        @role('warga')
            <div class="col-md-3">
                <!-- Profile Image -->
                <div class="box box-primary">
                    <!-- <div class="box-header with-border">
                        <h3 class="box-title">Surat Kelahiran</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i>
                            </button>
                      </div>
                    </div> -->
                    <div class="box-body box-profile">
                      <img class="profile-user-img img-responsive img-circle" src="{{ asset('/img/bg/pegawai.png') }}" alt="User profile picture" style="max-height: 100px;">

                      <h3 class="profile-username text-center">Kartu Pencari Kerja<br><br></h3>

                      <hr>
                      <p class="text-muted text-center">Kartu yang diisyaratkan untuk mendapatkan perkerjaan serta mendata...<br></p>
                      <p class="text-center">
                          
                      <a href="#" class="btn btn-info" data-toggle="modal" data-target="#modal-daftar-kpk"><b>Daftar</b></a>
                      <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-kpk"><b>Syarat</b></a>
                      <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-biaya"><b>Biaya</b></a>
                      </p>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            
            <div class="col-md-3">
                <!-- Profile Image -->
                <div class="box box-primary">
                    <!-- <div class="box-header with-border">
                        <h3 class="box-title">Surat Kelahiran</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i>
                            </button>
                      </div>
                    </div> -->
                    <div class="box-body box-profile">
                      <img class="profile-user-img img-responsive img-circle" src="{{ asset('/img/bg/du.png') }}" alt="User profile picture" style="max-height: 100px;">

                      <h3 class="profile-username text-center">Surat Keterangan Domisili Usaha</h3>

                      <hr>
                      <p class="text-muted text-center">Surat yang menyatakan domisili seseorang atau suatu badan usaha / perusahaan...</p>
                      <p class="text-center">
                          
                      <a href="#" class="btn btn-info" data-toggle="modal" data-target="#modal-dskdu"><b>Daftar</b></a>
                      <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-skdu"><b>Syarat</b></a>
                      <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-biaya"><b>Biaya</b></a>
                      </p>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

            <div class="col-md-3">
                <!-- Profile Image -->
                <div class="box box-primary">
                    <!-- <div class="box-header with-border">
                        <h3 class="box-title">Surat Kelahiran</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i>
                            </button>
                      </div>
                    </div> -->
                    <div class="box-body box-profile">
                      <img class="profile-user-img img-responsive img-circle" src="{{ asset('/img/bg/skm.png') }}" alt="User profile picture" style="max-height: 100px;">

                      <h3 class="profile-username text-center">Surat Kematian<br><br></h3>

                      <hr>
                      <p class="text-muted text-center">Surat kematian ialah surat yang berisi pernyataan bahwa seseorang telah dinyatakan…<br></p>
                      <p class="text-center">
                          
                      <a href="#" class="btn btn-info" data-toggle="modal" data-target="#modal-daftar-skm"><b>Daftar</b></a>
                      <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-skm"><b>Syarat</b></a>
                      <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-biaya"><b>Biaya</b></a>
                      </p>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

            <div class="col-md-3">
                <!-- Profile Image -->
                <div class="box box-primary">
                    <!-- <div class="box-header with-border">
                        <h3 class="box-title">Surat Kelahiran</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i>
                            </button>
                      </div>
                    </div> -->
                    <div class="box-body box-profile">
                      <img class="profile-user-img img-responsive img-circle" src="{{ asset('/img/bg/skdtt.jpg') }}" alt="User profile picture" style="max-height: 100px;">

                      <h3 class="profile-username text-center">Surat Keterangan Domisili Tempat Tinggal</h3>

                      <hr>
                      <p class="text-muted text-center">Surat Keterangan Domisili Tempat Tinggal adalah surat yang menyatakan alamat…<br></p>
                      <p class="text-center">
                          
                      <a href="#" class="btn btn-info" data-toggle="modal" data-target="#modal-daftar-skdtt"><b>Daftar</b></a>
                      <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-skdtt"><b>Syarat</b></a>
                      <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-biaya"><b>Biaya</b></a>
                      </p>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

            <div class="col-md-3">
                <!-- Profile Image -->
                <div class="box box-primary">
                    <!-- <div class="box-header with-border">
                        <h3 class="box-title">Surat Kelahiran</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i>
                            </button>
                      </div>
                    </div> -->
                    <div class="box-body box-profile">
                      <img class="profile-user-img img-responsive img-circle" src="{{ asset('/img/bg/skaw.png') }}" alt="User profile picture" style="max-height: 100px;">

                      <h3 class="profile-username text-center">Surat Keterangan Ahli Waris<br></h3>

                      <hr>
                      <p class="text-muted text-center">Surat Keterangan Ahli Waris atau Surat Keterangan Waris (SKW) adalah…<br></p>
                      <p class="text-center">
                          
                      <a href="#" class="btn btn-info" data-toggle="modal" data-target="#modal-dskaw"><b>Daftar</b></a>
                      <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-skaw"><b>Syarat</b></a>
                      <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-biaya"><b>Biaya</b></a>
                      </p>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

            <div class="col-md-3">
                <!-- Profile Image -->
                <div class="box box-primary">
                    <!-- <div class="box-header with-border">
                        <h3 class="box-title">Surat Kelahiran</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i>
                            </button>
                      </div>
                    </div> -->
                    <div class="box-body box-profile">
                      <img class="profile-user-img img-responsive img-circle" src="{{ asset('/img/bg/sku.png') }}" alt="User profile picture" style="max-height: 100px;">

                      <h3 class="profile-username text-center">Surat Keterangan Usaha<br><br></h3>

                      <hr>
                      <p class="text-muted text-center">Surat Keterangan Usaha (SKU) adalah surat yang menjelaskan secara resmi jenis usaha...<br></p>
                      <p class="text-center">
                          
                      <a href="#" class="btn btn-info" data-toggle="modal" data-target="#modal-daftar-sku"><b>Daftar</b></a>
                      <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-sku"><b>Syarat</b></a>
                      <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-biaya"><b>Biaya</b></a>
                      </p>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        @endrole
        @if(Auth::guest())
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="box box-primary">
                <!-- <div class="box-header with-border">
                    <h3 class="box-title">Surat Kelahiran</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i>
                        </button>
                  </div>
                </div> -->
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="{{ asset('/img/bg/pegawai.png') }}" alt="User profile picture" style="max-height: 100px;">

                  <h3 class="profile-username text-center">Kartu Pencari Kerja<br><br></h3>

                  <hr>
                  <p class="text-muted text-center">Kartu yang diisyaratkan untuk mendapatkan perkerjaan serta mendata...<br></p>
                  <p class="text-center">
                      
                  <a href="{{ route('login') }}" class="btn btn-info"><b>Daftar</b></a>
                  <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-kpk"><b>Syarat</b></a>
                  <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-biaya"><b>Biaya</b></a>
                  </p>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="box box-primary">
                <!-- <div class="box-header with-border">
                    <h3 class="box-title">Surat Kelahiran</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i>
                        </button>
                  </div>
                </div> -->
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="{{ asset('/img/bg/du.png') }}" alt="User profile picture" style="max-height: 100px;">

                  <h3 class="profile-username text-center">Surat Keterangan Domisili Usaha</h3>

                  <hr>
                  <p class="text-muted text-center">Surat yang menyatakan domisili seseorang atau suatu badan usaha / perusahaan...</p>
                  <p class="text-center">
                      
                  <a href="{{ route('login') }}" class="btn btn-info"><b>Daftar</b></a>
                  <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-skdu"><b>Syarat</b></a>
                  <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-biaya"><b>Biaya</b></a>
                  </p>
                </div>
                <!-- /.box-body -->
            </div>
        </div>

        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="box box-primary">
                <!-- <div class="box-header with-border">
                    <h3 class="box-title">Surat Kelahiran</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i>
                        </button>
                  </div>
                </div> -->
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="{{ asset('/img/bg/skm.png') }}" alt="User profile picture" style="max-height: 100px;">

                  <h3 class="profile-username text-center">Surat Kematian<br><br></h3>

                  <hr>
                  <p class="text-muted text-center">Surat kematian ialah surat yang berisi pernyataan bahwa seseorang telah dinyatakan…<br></p>
                  <p class="text-center">
                      
                  <a href="{{ route('login') }}" class="btn btn-info"><b>Daftar</b></a>
                  <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-skm"><b>Syarat</b></a>
                  <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-biaya"><b>Biaya</b></a>
                  </p>
                </div>
                <!-- /.box-body -->
            </div>
        </div>

        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="box box-primary">
                <!-- <div class="box-header with-border">
                    <h3 class="box-title">Surat Kelahiran</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i>
                        </button>
                  </div>
                </div> -->
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="{{ asset('/img/bg/skdtt.jpg') }}" alt="User profile picture" style="max-height: 100px;">

                  <h3 class="profile-username text-center">Surat Keterangan Domisili Tempat Tinggal</h3>

                  <hr>
                  <p class="text-muted text-center">Surat Keterangan Domisili Tempat Tinggal adalah surat yang menyatakan alamat…<br></p>
                  <p class="text-center">
                      
                  <a href="{{ route('login') }}" class="btn btn-info"><b>Daftar</b></a>
                  <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-skdtt"><b>Syarat</b></a>
                  <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-biaya"><b>Biaya</b></a>
                  </p>
                </div>
                <!-- /.box-body -->
            </div>
        </div>

        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="box box-primary">
                <!-- <div class="box-header with-border">
                    <h3 class="box-title">Surat Kelahiran</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i>
                        </button>
                  </div>
                </div> -->
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="{{ asset('/img/bg/skaw.png') }}" alt="User profile picture" style="max-height: 100px;">

                  <h3 class="profile-username text-center">Surat Keterangan Ahli Waris<br></h3>

                  <hr>
                  <p class="text-muted text-center">Surat Keterangan Ahli Waris atau Surat Keterangan Waris (SKW) adalah…<br><br></p>
                  <p class="text-center">
                      
                  <a href="{{ route('login') }}" class="btn btn-info"><b>Daftar</b></a>
                  <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-skaw"><b>Syarat</b></a>
                  <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-biaya"><b>Biaya</b></a>
                  </p>
                </div>
                <!-- /.box-body -->
            </div>
        </div>

        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="box box-primary">
                <!-- <div class="box-header with-border">
                    <h3 class="box-title">Surat Kelahiran</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i>
                        </button>
                  </div>
                </div> -->
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="{{ asset('/img/bg/sku.png') }}" alt="User profile picture" style="max-height: 100px;">

                  <h3 class="profile-username text-center">Surat Keterangan Usaha<br><br></h3>

                  <hr>
                  <p class="text-muted text-center">Surat Keterangan Usaha (SKU) adalah surat yang menjelaskan secara resmi jenis usaha...<br></p>
                  <p class="text-center">
                      
                  <a href="{{ route('login') }}" class="btn btn-info"><b>Daftar</b></a>
                  <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modal-sku"><b>Syarat</b></a>
                  <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-biaya"><b>Biaya</b></a>
                  </p>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        @endif
    </div>
    @include('kpk')
    @include('dkpk')
    @include('biaya')
    @include('skaw')
    @include('dskaw')
    @include('skdtt')
    @include('dskdtt')
    @include('skdu')
    @include('dskdu')
    @include('skm')
    @include('dskm')
    @include('sku')
    @include('dsku')
@endsection

