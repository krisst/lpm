<html>
<head>
    <link rel="stylesheet" href="{{ asset('/css/custom.css') }}">
    <title>Print</title>
    <style>
    @media print {  
      @page { size: A4;
         margin:1cm;
       }
      .sheet {
        overflow: visible;
        height: auto !important;
        page-break-inside:avoid; page-break-after:auto;
        page-break-before: always;
      }


      h1 {
        font-size: 12pt;
        margin-bottom: 4px;
      }
    }

    @media screen{
      @page { size: A4; margin:1cm !important; }
      .sheet {
        overflow: visible;
        height: auto !important;
        page-break-inside:avoid; page-break-after:auto;
        page-break-before: always;
        padding-left:6cm;
        padding-top:1cm;
        padding-right:6cm;
      }

      h1 {
        font-size: 12pt;
        margin-bottom: 4px;
      }
    }
    
    body {
        font-family: Trebuchet MS,Arial;
        font-size: 12px;
    }

    </style>
</head>

<body class="A4">
  <section class="sheet">
    @yield('content')
  </section>
</body>
<script src="{{ asset('/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script type="text/javascript">
    $(function () {
        // printInvoice();
    });
    
    function printInvoice() {
        window.print();
    }
  </script>
</html>