<div class="modal fade" id="modal-skaw">
  	<div class="modal-dialog">
        <div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		      		<span aria-hidden="true">&times;</span>
		  		</button>
		    	<h4 class="modal-title">Persyaratan Surat Keterangan Domisili Usaha</h4>
		  	</div>
		  	<div class="modal-body">
		        <div class="row">
		        	<div class="col-md-12 table-responsive">
		        		<p>
		        			Surat Keterangan Ahli Waris atau Surat Keterangan Waris (SKW) adalah surat yang dibutuhkan untuk melengkapi persyaratan atau kelengkapan administratif dalam urusan tertentu. Pengurusan ini dilakukan di Kelurahan lalu melanjutkan ke Kecamatan. 
		        		</p><br>
		        		<table class="table table-condensed table-hover table-striped table-bordered">
		        			<thead>
		        				<tr>
		        					<th width="5%" class="text-right">No.</th>
		        					<th width="15%" class="text-center">Ketentuan</th>
		        					<th>Syarat</th>
		        				</tr>
		        			</thead>
		        			<tbody>
		        				<tr>
		        					<td class="text-right">1.</td>
		        					<td class="text-center">Wajib</td>
		        					<td>Surat Pengantar RT/RW</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">2.</td>
		        					<td class="text-center">Wajib</td>
		        					<td>Surat Pernyataan Ahli Waris</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">3.</td>
		        					<td class="text-center">Wajib</td>
		        					<td>KK Pewaris</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">4.</td>
		        					<td class="text-center">Wajib</td>
		        					<td>KTP Pewaris</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">5.</td>
		        					<td class="text-center">Wajib</td>
		        					<td>Surat Pencatatan Sipil Akta Kematian</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">6.</td>
		        					<td class="text-center">Wajib</td>
		        					<td>Akta Lahir Ahli Waris yang belum cukup umur</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">7.</td>
		        					<td class="text-center">Wajib</td>
		        					<td>Surat Keterangan Kematian dari Kelurahan</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">8.</td>
		        					<td class="text-center">Wajib</td>
		        					<td>Surat Nikah</td>
		        				</tr>
		        				<tr>
		        					<td class="text-right">9.</td>
		        					<td class="text-center">Tidak Wajib</td>
		        					<td>KTP masing-masing Ahli Waris</td>
		        				</tr>
		        			</tbody>
		        		</table>
		  			</div>
		        </div>
		  	</div>
		    <div class="modal-footer">
		        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
		    </div>
    
    	</div>
  	</div>
</div>