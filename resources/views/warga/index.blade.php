@extends('templates.base')

@section('content')
    <section class="content-header">
      <h1>
            DATA WARGA
            <!-- <small>Control panel</small> -->
        </h1>
      <ol class="breadcrumb">
          <li><a href="{{ route('home') }}"><i class="fa fa-home"></i> Beranda</a></li>
          <li> {{ $title }}</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
          <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">&nbsp;</h3>

                  <div class="box-tools">
                    <!-- <form method="get">
                      <div class="input-group input-group-sm" style="width: 300px;">
                          <input type="text" name="keyword" class="form-control pull-right" value="@if(isset($keyword)){{$keyword}}@endif" placeholder="Search">

                          <div class="input-group-btn">
                              <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                    </form> -->
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                  <!-- <a class="btn btn-sm btn-success" href=""><i class="fa fa-plus"></i> New User</a><br> -->
                  <table id="example1" class="table table-hover table-condensed table-striped">
                      <thead>
                        <tr>
                          <th class="text-right">No</th>
                          <th class="">NIK</th>
                          <th>Nama</th>
                          <th>Ttl</th>
                          <th class="">Phone</th>
                          <th>Email</th>
                          <th>Alamat</th>
                          <th class="text-center">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(isset($warga))
                          <?php $i = 1; ?>
                          @foreach($warga as $st)
                            <tr>
                                <td class="text-right">{{ $i++ }}</td>
                                <td class="">{{ $st->nik }}</td>
                                <td>{{ $st->name }}</td>
                                <td>{{ $st->ttl }}, {{ date('d-M-Y',strtotime($st->birth_date)) }}</td>
                                <td class="">{{ $st->phone }}</td>
                                <td>{{ $st->email }}</td>
                                <td>{{ $st->address }}</td>
                                <td width="10%" class="text-center">
                                    <a href="{{ route('warga.show', ['id_pengajuan' => $st->id]) }}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                          @endforeach
                        @else
                          <tr>
                              <td colspan="7" class="text-center">No Data Result!</td>
                          </tr>
                        @endif
                      </tbody>
                  </table>

                </div>
                <!-- /.box-body -->
            </div>
              <!-- /.box -->
          </div>
      </div>
    </section>
@endsection

