<div class="modal fade" id="modal-dskdu">
  	<div class="modal-dialog">
        <div class="modal-content">
		  	<div class="modal-header bg-primary">
		    	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		      		<span aria-hidden="true">&times;</span>
		  		</button>
		    	<h4 class="modal-title">Pendaftaran Surat Keterangan Domisili Usaha</h4>
		  	</div>
		  	<div class="modal-body">
		        <div class="row">
		        	<div class="col-md-12 table-responsive">
		        		<p>
		        			Pendaftaran untuk Surat Keterangan Domisili Usaha ini harus datang langsung ke tempat kantor kami, serta membawa semua dokumen persyaratannya.
		        		</p><br>
		  			</div>
		        </div>
		  	</div>
		    <div class="modal-footer">
		        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
		    </div>
    
    	</div>
  	</div>
</div>