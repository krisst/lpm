<?php

namespace App;

use Laratrust\LaratrustRole;

class Role extends LaratrustRole
{
    protected $table='roles';

    public function roleUser(){
        return $this->hasMany('App\RoleUser');
    } 

    public function users()
    {
        return $this->belongsToMany('User');
    } 
}
