<?php

namespace App\Http\Middleware;

use  Closure;
use Illuminate\Support\Facedes\Auth as Auth;

/**
* 
*/
class CheckUser
{
	
	public function handle($request, Closure $next, $level)
	{
		$user = Auth::user();
		if ($user && $user->level != $level) {
			return redirect('/');
		}
		else return $next($request);
	}
}