<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;

class CountriesController extends Controller
{
    public function loadCountries(Request $request)
    {
        # code...
        $term = trim($request->q);
        if (empty($term)) {
            $countries = Country::orderBy('country_name', 'asc')->limit(20)->get();
        } else {
            $countries = Country::where('country_name', 'LIKE', '%'.$term.'%')->orderBy('country_name', 'asc')->limit(20)->get();
        }

        $data = [];
        foreach ($countries as $dt) {
            $data[] = ['id' => $dt->country_id, 'text' => $dt->country_name];
        }
        return \Response::json($data);
    }
}
