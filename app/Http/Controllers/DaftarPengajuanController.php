<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengajuan;
use Session;
use File;
use Illuminate\Support\Facades\Auth;

class DaftarPengajuanController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->userId = Auth::user()->id;
            $this->userName = Auth::user()->name;
            $this->roleId = Auth::user()->roleUser[0]->role_id;
            return $next($request);
        });

        $this->imagePath =  public_path() . DIRECTORY_SEPARATOR . 'gambar/berkas';
    }

    public function skm(Request $request)
    {
        $this->Validate($request, [
            'ktp' => 'required|mimes:jpeg,png,jpg|max:2048',
            'spr' => 'required|mimes:jpeg,png,jpg|max:2048',
            'kk' => 'required|mimes:jpeg,png,jpg|max:2048',
        ]);

        $now = date('Y-m-d');
        $pengajuan = Pengajuan::create(['user_id' => $this->userId,'tgl_pengajuan' => $now, 'jenis_pengajuan' => $request->jenis_pengajuan, 'status' => 0, 'nama_alm' => $request->nama_alm, 'alamat_alm' => $request->alamat_alm, 'tgl_meninggal' => $request->tgl_meninggal]);

        if ($request->hasFile('ktp')) {
            $filename = null;
            $upload_image = $request->file('ktp');
            $extension = $upload_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = date('mdYHis') . uniqid().'.'.$extension;

            // memindahkan file ke folder public/img
            $upload_image->move($this->imagePath, $filename);
            $pengajuan->ktp = $filename;
        }

        if ($request->hasFile('kk')) {
            $filename = null;
            $upload_image = $request->file('kk');
            $extension = $upload_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = date('mdYHis') . uniqid().'.'.$extension;

            // memindahkan file ke folder public/img
            $upload_image->move($this->imagePath, $filename);
            $pengajuan->kk = $filename;
        }

        if ($request->hasFile('spr')) {
            $filename = null;
            $upload_image = $request->file('spr');
            $extension = $upload_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = date('mdYHis') . uniqid().'.'.$extension;

            // memindahkan file ke folder public/img
            $upload_image->move($this->imagePath, $filename);
            $pengajuan->spr = $filename;
        }

        $pengajuan->save();

        Session::flash("flash_notification", [
            "level"=>"info",
            "message"=>"Pengajuan sudah terkirim"
        ]);
        return redirect()->route('pengajuan.index');
    }

    public function skdtt(Request $request)
    {
        $this->Validate($request, [
            'ktp' => 'required|mimes:jpeg,png,jpg|max:2048',
            'spr' => 'required|mimes:jpeg,png,jpg|max:2048',
            'kk' => 'required|mimes:jpeg,png,jpg|max:2048',
        ]);

        $now = date('Y-m-d');
        $pengajuan = Pengajuan::create(['user_id' => $this->userId,'tgl_pengajuan' => $now, 'jenis_pengajuan' => $request->jenis_pengajuan, 'status' => 0, 'nama_alm' => $request->nama_alm, 'alamat_alm' => $request->alamat_alm, 'tgl_meninggal' => $request->tgl_meninggal]);

        if ($request->hasFile('ktp')) {
            $filename = null;
            $upload_image = $request->file('ktp');
            $extension = $upload_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = date('mdYHis') . uniqid().'.'.$extension;

            // memindahkan file ke folder public/img
            $upload_image->move($this->imagePath, $filename);
            $pengajuan->ktp = $filename;
        }

        if ($request->hasFile('kk')) {
            $filename = null;
            $upload_image = $request->file('kk');
            $extension = $upload_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = date('mdYHis') . uniqid().'.'.$extension;

            // memindahkan file ke folder public/img
            $upload_image->move($this->imagePath, $filename);
            $pengajuan->kk = $filename;
        }

        if ($request->hasFile('spr')) {
            $filename = null;
            $upload_image = $request->file('spr');
            $extension = $upload_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = date('mdYHis') . uniqid().'.'.$extension;

            // memindahkan file ke folder public/img
            $upload_image->move($this->imagePath, $filename);
            $pengajuan->spr = $filename;
        }

        $pengajuan->save();

        Session::flash("flash_notification", [
            "level"=>"info",
            "message"=>"Pengajuan sudah terkirim"
        ]);
        return redirect()->route('pengajuan.index');
    }

    public function sku(Request $request)
    {
        $this->Validate($request, [
            'ktp' => 'required|mimes:jpeg,png,jpg|max:2048',
            'spr' => 'required|mimes:jpeg,png,jpg|max:2048',
            'kk' => 'required|mimes:jpeg,png,jpg|max:2048',
        ]);

        $now = date('Y-m-d');
        $pengajuan = Pengajuan::create(['user_id' => $this->userId,'tgl_pengajuan' => $now, 'jenis_pengajuan' => $request->jenis_pengajuan, 'status' => 0, 'nama_alm' => $request->nama_alm, 'alamat_alm' => $request->alamat_alm]);

        if ($request->hasFile('ktp')) {
            $filename = null;
            $upload_image = $request->file('ktp');
            $extension = $upload_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = date('mdYHis') . uniqid().'.'.$extension;

            // memindahkan file ke folder public/img
            $upload_image->move($this->imagePath, $filename);
            $pengajuan->ktp = $filename;
        }

        if ($request->hasFile('kk')) {
            $filename = null;
            $upload_image = $request->file('kk');
            $extension = $upload_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = date('mdYHis') . uniqid().'.'.$extension;

            // memindahkan file ke folder public/img
            $upload_image->move($this->imagePath, $filename);
            $pengajuan->kk = $filename;
        }

        if ($request->hasFile('spr')) {
            $filename = null;
            $upload_image = $request->file('spr');
            $extension = $upload_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = date('mdYHis') . uniqid().'.'.$extension;

            // memindahkan file ke folder public/img
            $upload_image->move($this->imagePath, $filename);
            $pengajuan->spr = $filename;
        }

        $pengajuan->save();

        Session::flash("flash_notification", [
            "level"=>"info",
            "message"=>"Pengajuan sudah terkirim"
        ]);
        return redirect()->route('pengajuan.index');
    }
}
