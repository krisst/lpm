<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nik' => 'required|string|max:16',
            'name' => 'required|string|max:150',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|max:10',
            'password_confirmation' => 'same:password',
            'gender' => 'required',
            'ttl' => 'required|string|max:15',
            'birth_date' => 'required',
            'country_id' => 'required',
            'religion' => 'required',
            'status_perkawinan' => 'required',
            'pekerjaan' => 'required|string|max:150',
            'address' => 'required|string|max:150',
            'rt' => 'required',
            'rw' => 'required',
            'kelurahan' => 'required|string|max:50',
            'kecamatan' => 'required|string|max:50',
            'phone' => 'required|max:13',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'nik' => $data['nik'],
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'gender' => $data['gender'],
            'ttl' => $data['ttl'],
            'birth_date' => $data['birth_date'],
            'country_id' => $data['country_id'],
            'religion' => $data['religion'],
            'status_perkawinan' => $data['status_perkawinan'],
            'pekerjaan' => $data['pekerjaan'],
            'address' => $data['address'],
            'rt' => $data['rt'],
            'rw' => $data['rw'],
            'kelurahan' => $data['kelurahan'],
            'kecamatan' => $data['kecamatan'],
            'phone' => $data['phone'],
            'is_verified' => True,
        ]);

        $user->syncRoles([2]);

        return redirect('/login');
    }
}
