<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class WargaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $keyword = $request->get('keyword');
        $warga = User::orderBy('name','asc')->get();
        $title = "Daftar Warga";   
            
        return view('warga.index', compact('title','warga'));
    }

    public function show($id)
    {
        // $keyword = $request->get('keyword');
        $warga = User::find($id);
        $title = "Detail Warga";   
            
        return view('warga.show', compact('title','warga'));
    }
}
