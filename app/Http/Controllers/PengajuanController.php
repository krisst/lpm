<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengajuan;
use Session;
use File;
use Illuminate\Support\Facades\Auth;

class PengajuanController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->userId = Auth::user()->id;
            $this->userName = Auth::user()->name;
            $this->roleId = Auth::user()->roleUser[0]->role_id;
            return $next($request);
        });

        $this->imagePath =  public_path() . DIRECTORY_SEPARATOR . 'gambar/berkas';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title              = "Daftar Surat Pengajuan";
        $pengajuan          = Pengajuan::with('user');

        if ($this->roleId == 2){
            $pengajuan = $pengajuan->where('user_id', $this->userId);
        }

        $pengajuan = $pengajuan->get();
        return view('pengajuan.index', compact('title', 'pengajuan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->Validate($request, [
            'foto' => 'required|mimes:jpeg,png,jpg|max:2048',
            'ktp' => 'required|mimes:jpeg,png,jpg|max:2048',
            'ijazah' => 'required|mimes:jpeg,png,jpg|max:2048',
            'kk' => 'required|mimes:jpeg,png,jpg|max:2048',
        ]);

        $now = date('Y-m-d');
        $pengajuan = Pengajuan::create(['user_id' => $this->userId,'tgl_pengajuan' => $now, 'jenis_pengajuan' => $request->jenis_pengajuan, 'status' => 0]);

        if ($request->hasFile('foto')) {
            $filename = null;
            $upload_image = $request->file('foto');
            $extension = $upload_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = date('mdYHis') . uniqid().'.'.$extension;

            // memindahkan file ke folder public/img
            $upload_image->move($this->imagePath, $filename);
            $pengajuan->foto = $filename;
        }

        if ($request->hasFile('ktp')) {
            $filename = null;
            $upload_image = $request->file('ktp');
            $extension = $upload_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = date('mdYHis') . uniqid().'.'.$extension;

            // memindahkan file ke folder public/img
            $upload_image->move($this->imagePath, $filename);
            $pengajuan->ktp = $filename;
        }

        if ($request->hasFile('kk')) {
            $filename = null;
            $upload_image = $request->file('kk');
            $extension = $upload_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = date('mdYHis') . uniqid().'.'.$extension;

            // memindahkan file ke folder public/img
            $upload_image->move($this->imagePath, $filename);
            $pengajuan->kk = $filename;
        }

        if ($request->hasFile('ijazah')) {
            $filename = null;
            $upload_image = $request->file('ijazah');
            $extension = $upload_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = date('mdYHis') . uniqid().'.'.$extension;

            // memindahkan file ke folder public/img
            $upload_image->move($this->imagePath, $filename);
            $pengajuan->ijazah = $filename;
        }

        $pengajuan->save();

        Session::flash("flash_notification", [
            "level"=>"info",
            "message"=>"Pengajuan sudah terkirim"
        ]);
        return redirect()->route('pengajuan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pengajuan              = Pengajuan::find($id);
        $title                  = "";
        if ($pengajuan->jenis_pengajuan == "kpk") $title = "KARTU PENCARI KERJA"; 
        if ($pengajuan->jenis_pengajuan == "skm") $title = "SURAT KEMATIAN"; 
        if ($pengajuan->jenis_pengajuan == "sku") $title = "SURAT KETERANGAN USAHA"; 
        if ($pengajuan->jenis_pengajuan == "skdtt") $title = "SURAT KETERANGAN DOMISILI TEMPAT TINGGAL"; 
        return view('pengajuan.show', compact('pengajuan', 'title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function skm(Request $request)
    {
        $this->Validate($request, [
            'ktp' => 'required|mimes:jpeg,png,jpg|max:2048',
            'spr' => 'required|mimes:jpeg,png,jpg|max:2048',
            'kk' => 'required|mimes:jpeg,png,jpg|max:2048',
        ]);

        $now = date('Y-m-d');
        $pengajuan = Pengajuan::create(['user_id' => $this->userId,'tgl_pengajuan' => $now, 'jenis_pengajuan' => $request->jenis_pengajuan, 'status' => 0, 'nama_alm' => $request->nama_alm, 'alamat_alm' => $request->alamat_alm, 'tgl_meninggal' => $request->tgl_meninggal]);

        if ($request->hasFile('ktp')) {
            $filename = null;
            $upload_image = $request->file('ktp');
            $extension = $upload_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = date('mdYHis') . uniqid().'.'.$extension;

            // memindahkan file ke folder public/img
            $upload_image->move($this->imagePath, $filename);
            $pengajuan->ktp = $filename;
        }

        if ($request->hasFile('kk')) {
            $filename = null;
            $upload_image = $request->file('kk');
            $extension = $upload_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = date('mdYHis') . uniqid().'.'.$extension;

            // memindahkan file ke folder public/img
            $upload_image->move($this->imagePath, $filename);
            $pengajuan->kk = $filename;
        }

        if ($request->hasFile('spr')) {
            $filename = null;
            $upload_image = $request->file('spr');
            $extension = $upload_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = date('mdYHis') . uniqid().'.'.$extension;

            // memindahkan file ke folder public/img
            $upload_image->move($this->imagePath, $filename);
            $pengajuan->spr = $filename;
        }

        $pengajuan->save();

        Session::flash("flash_notification", [
            "level"=>"info",
            "message"=>"Pengajuan sudah terkirim"
        ]);
        return redirect()->route('pengajuan.index');
    }

    public function proses(Request $request)
    {
        $id_pengajuan = $request->id_pengajuan;
        $pengajuan = Pengajuan::where("id_pengajuan", $id_pengajuan)->first();

        $pengajuan->status = 1;
        $pengajuan->save();
        Session::flash("flash_notification", [
            "level"=>"info",
            "message"=>"Pengajuan disetujui"
        ]);
        return redirect()->route('pengajuan.index');
        // try {
        //     File::delete($this->imagePath.'/'.$item->item_picture);
        // }catch(FileNotFoundException $e) {
        //     // File sudah dihapus/tidak ada
        // }
    }

    public function tolak(Request $request)
    {
        $id_pengajuan = $request->id_pengajuan;
        $pengajuan = Pengajuan::where("id_pengajuan", $id_pengajuan)->first();

        $pengajuan->status = 2;
        $pengajuan->save();
        Session::flash("flash_notification", [
            "level"=>"danger",
            "message"=>"Pengajuan ditolak"
        ]);
        return redirect()->route('pengajuan.index');
        // try {
        //     File::delete($this->imagePath.'/'.$item->item_picture);
        // }catch(FileNotFoundException $e) {
        //     // File sudah dihapus/tidak ada
        // }
    }

    public function printSurat($id)
    {
        $pengajuan = Pengajuan::where('id_pengajuan', $id)->where('status', 1)->first();
        if ($pengajuan->jenis_pengajuan == "kpk") return view('pengajuan.kpk', compact('pengajuan'));
        if ($pengajuan->jenis_pengajuan == "skm") return view('pengajuan.skm', compact('pengajuan'));
        if ($pengajuan->jenis_pengajuan == "skdtt") return view('pengajuan.skdtt', compact('pengajuan'));
        if ($pengajuan->jenis_pengajuan == "sku") return view('pengajuan.sku', compact('pengajuan'));
    }
}
