<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengajuan extends Model
{
	protected $table = 'pengajuan';
    protected $primaryKey = 'id_pengajuan';

    protected $fillable = [
        'user_id', 'tgl_pengajuan', 'jenis_pengajuan', 'status', 'foto', 'ktp', 'ijazah', 'spkel', 'spku', 'sketdl', 'ssempadan', 'kk', 'spr', 'spaw', 'spsak', 'alawybcu', 'skkdk', 'sn', 'tgl_meninggal', 'nama_alm', 'alamat_alm'
    ];

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }

    public function jenisPengajuan($val)
    {
    	$label = '';
    	if($val == "kpk"){
    		$label = "KARTU PENCARI KERJA";
    	} else if($val == "skm"){
            $label = "SURAT KEMATIAN";
        }else if($val == "sku"){
            $label = "SURAT KETERANGAN USAHA";
        }
        else if($val == "skdtt"){
            $label = "SURAT KETERANGAN DOMISLI TEMPAT TINGGAL";
        }
    	return $label;
    }

    public function statusPengajuan($val)
    {
    	$label = '';
    	if($val == 0){
    		$label = "Diajukan";
    	} elseif ($val == 1) {
    		$label = "Disetujui";
    	} else {
    		$label = "Ditolak";
    	}

    	return $label;
    }
}
