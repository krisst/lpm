<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'nik', 'gender', 'ttl', 'birth_date', 'country_id', 'religion', 'status_perkawinan', 'pekerjaan', 'address', 'rt', 'rw', 'kelurahan', 'kecamatan', 'phone', 'is_verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roleUser(){
        return $this->hasMany('App\RoleUser');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function country(){
        return $this->belongsTo('App\Country', 'country_id', 'country_id');
    }    
}
