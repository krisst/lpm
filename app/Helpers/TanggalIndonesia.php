<?php

function tanggalIndonesia($date, $show_day=true){
	$days = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
	$months = array(1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
	$year = substr($date, 0,4);
	$month = $months[(int)substr($days, 5, 2)];
	$dates = substr($date, 8, 2);

	$text = "";

	if ($show_day) {
		$list_of_day = date('w', mktime(0,0,0, substr($date, 5, 2), $dates, $year));
		$day = $days[$list_of_day];
		$text .= $day.", ";
	}


	$text .= $day." ".$month." ".$year;
	return $text;
}